pipeline {

    options {
        timeout(time: 60, unit: 'MINUTES')
    }
    
    environment {
        imageName = 'jcdark/iesp-jenkins'
        registryCredential = 'dockerhub'
    }

    agent none

    stages {
        stage('build') {
            agent {
                docker {
                    image 'node'
                }
            }
            steps {
                dir('website') {
                    sh 'npm install'
                    sh 'npm run build'
                }
            }
            post {
                success {
                    archiveArtifacts artifacts: 'website/build/test-site'
                }
            }
        }
        stage('tests') {
            parallel {
                stage('code-analysis') {
                    agent {
                        docker {
                            image 'node'
                        }
                    }
                    steps {
                        dir('website') {
                            sh 'npm install --global sonarqube-scanner'
                            sh 'node sonar-project.js'
                        }
                    }                        
                }
                stage('functional') {
                    agent {
                        docker {
                            image 'node'
                        }
                    }
                    steps {
                        dir('website') {
                            sh 'npm install -g broken-link-checker wait-on'
                            sh 'setsid npm run start >/dev/null 2>&1 < /dev/null &'
                        }
                    }
                }
                stage('generate image') {
                    agent {
                        docker {
                            image 'node'
                        }
                    }
                    steps {
                        script {
                            dockerImage = docker.build imageName
                        }
                    }
                }

                stage('push image') {
                    agent {
                        docker {
                            image 'node'
                        }
                    }
                    steps {
                        script {
                            docker.withRegistry('', registryCredential) {
                                dockerImage.push()
                            }
                        }
                    }
                }
            }
            
        }
    }
}